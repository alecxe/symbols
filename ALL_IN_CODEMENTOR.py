
# coding: utf-8

# In[1]:


import datetime
import pickle
import os
import time

from pandas_datareader import data as web
import pandas as pd


start = '2006-01-01'
today = end = pd.to_datetime('today')


# In[2]:


# THIS BLOCK OF CODE IS ALREADY OPTIMIZED

df = pd.read_csv("csv_stocks.csv")


def save_tslag(symbol, df, lags=252, lb=21):
    """Saves a tslag dataframe."""
    tslag = df
    for index in range(lags):
        tslag[f"Lag_{index + 1}"] = tslag["Adj Close"].shift(index + 1)
    tslag["return"] = (1 / ((tslag["Adj Close"]).pct_change(-lb) + 1)) - 1
    tslag.to_pickle(f"symbols_data/tslag[{symbol}].pkl")


def fetch_data(symbols, start, end, limit=10000):
    """
    Download data from yahoo for the given list of symbols
    The data is converted to pandas data frames and then pickled. Sometimes
    there maybe errors in fetching the data. This errors are added to the 
    `missed` list and you may call this method again with that list.
    
    The optional limit flag can be used to limit the number of symbols fetched.
    """
    end_datestring = end.strftime('%Y-%m-%d')  # pre-calculate
    missed = []
    for looper, symbol in enumerate(symbols):
        if looper >= limit:
            break
        
        path = f"symbols_data/df[{symbol}].pkl"
        if os.path.exists(path):
            old = pd.read_pickle(path)
            dt = old.index[-1].date() + datetime.timedelta(days = 1)
            start_date = dt.strftime('%Y-%m-%d')
            old_data = True
        else:
            start_date = start
            old_data = False
        
        if start_date == end_datestring:
            print('{0:4} cached'.format(symbol))
            save_tslag(symbol, old)
            continue
        
        try:
            print("{0:3}: {1:4} {2} {3}".format(looper, symbol, start_date, end))
            data = web.get_data_yahoo(symbol, start_date ,end)

            df2 = old.append(data[data.index >= start_date]) if old_data else data
            df2.to_pickle(path)
            
            save_tslag(symbol, df2)
        
        except:
            import traceback
            traceback.print_exc()
            print(f"No information for ticker '{symbol}'")
            missed.append(symbol)
            continue

    return missed

start_time = datetime.datetime.now()
missed = fetch_data(df.Symbol, start, end)
end_time = datetime.datetime.now()

print(f'Duration: {end_time - start_time}')

if missed:
    # reload missing data, since our system is efficient and the code modularized
    # we can now retry missing data several times if needed.
    missed = fetch_data(missed, start, end)


# In[3]:


# THIS CODE NEEDS OPTIMIZATION AND INCREASE SPEED

# IN THIS SECTION WE TRY TO DEVELOP STATISTIC/RISK MATRIX FOR EACH STOCK
# IN ORDER TO HAVE MEAN REVERSION STRATEGIES


import numpy as np

start_time = datetime.datetime.now()


symbols = pd.read_pickle("symbols_data/symbols.pkl")

df = {symbol: pd.DataFrame() for symbol in symbols}

today = pd.to_datetime('today')
end = today

bs = 21
fw = 21


for symbol in symbols:
    df[symbol] = pd.read_pickle("symbols_data/df[{}].pkl".format(symbol))
    df[symbol]["return"] = df[symbol]["Adj Close"].pct_change(bs)
    df[symbol] = df[symbol].dropna()





general_db = pd.DataFrame (index=df["^NDX"].index, columns = symbols)

for symbol in symbols:
    general_db.ix[:,symbol] = df[symbol]["return"]

general_db.to_pickle("X_test/general_db")





# describe contains 21days return based risk matrix of each stock

describe = pd.DataFrame(index=symbols, columns=["count","mean","std","min","25%","50%","75%","max"])
describe = describe.T
count = 0
for col in general_db.columns:
    describe.ix[:,col] = general_db[col].describe()





# minimis contains 21days cycles return=21st day / min of the 21 days cycle e.g if value is 0 the
#minimum of the 21days cycle was reached the 21day

minimis = pd.DataFrame(index=general_db["^NDX"].index,columns=symbols)


for symbol in symbols:
    df[symbol] = pd.read_pickle("symbols_data/df[{}].pkl".format(symbol))
    day = 0
    for day in range(bs,df[symbol].shape[0]):
        slices = df[symbol]["Adj Close"].ix[(day-bs):day]
        num = (slices[-1]/slices.min())-1
        minimis.ix[(day-bs),symbol] = num





# maximis contains 21days cycles return= max / 21st day of the 21 days cycle

maximis = pd.DataFrame(index=general_db["^NDX"].index,columns=symbols)


for symbol in symbols:
    day = 0
    for day in range(bs,df[symbol].shape[0]):
        slices = df[symbol]["Adj Close"].ix[(day-bs):day]
        num = (slices.max() / slices[-1])-1
        maximis.ix[(day-bs),symbol] = num





# let's set now take profit and stop loss limits

stop_loss = pd.DataFrame(index=["STOP_LOSS"],columns=symbols)
take_profit = pd.DataFrame(index=["TAKE_PROFIT"],columns=symbols)


for symbol in symbols:
    stop_loss[symbol] = minimis[symbol].mean()
    take_profit[symbol] = maximis[symbol].mean()

stop_loss.to_pickle("symbols_data/stop_loss")
take_profit.to_pickle("symbols_data/take_profit")





line = np.sign(general_db).values
directions = pd.DataFrame(data=line,columns=symbols)
recap=pd.DataFrame(index=range(-20,20),columns=range(1,22))
consecutives = pd.DataFrame(index=range(-20,20),columns=symbols)
desc = range(directions.shape[0],directions.shape[0]-bs,-1)
temp = pd.DataFrame()
temp2 =pd.DataFrame()
temp3 =pd.DataFrame()


for symbol in symbols:
    print (symbol),
    count = 0
    for i in desc:
        temp = directions.reindex(index=directions.index[::-21])
        temp2 = temp[::-1]
        temp3 = temp2[symbol]
        temp3["CONS"] = temp2[symbol] * (temp2[symbol].groupby((temp2[symbol]!=temp2[symbol].shift()).cumsum()).cumcount() + 1)

        dati = temp3.groupby(["CONS"]).count()

        recap[count] = pd.DataFrame(data=dati)
        directions = directions.iloc[:i-1,:]
        count +=1
    recap["TRANS"] = recap.mean(axis=1)
    recap["prob"] = recap["TRANS"] / recap["TRANS"].sum()*100
    consecutives[symbol] = recap["prob"]
consecutives.to_pickle("symbols_data/consecutives")


#current status

line = np.sign(general_db).values
directions = pd.DataFrame(data=line,columns=symbols)
recap=pd.DataFrame(columns=range(1,22))
current = pd.DataFrame(columns=symbols)
desc = range(directions.shape[0],directions.shape[0]-bs,-1)
temp = pd.DataFrame()
temp2 =pd.DataFrame()
temp3 =pd.DataFrame()


for symbol in symbols:
    print (symbol),
    count = 0
    for i in desc:
        temp = directions.reindex(index=directions.index[::-21])
        temp2 = temp[::-1]
        temp3 = temp2[symbol]

        temp3["CONS"] = temp2[symbol] * (temp2[symbol].groupby((temp2[symbol]!=temp2[symbol].shift()).cumsum()).cumcount() + 1)



        recap.iloc[:,count] = temp3["CONS"][-21:].values


        directions = directions.iloc[:i-1,:]
        count +=1
    recap["TRANS"] = recap.mode(axis=1)[0]
    #recap["prob"] = recap["TRANS"] / recap["TRANS"].sum()*100
    current[symbol] = recap.TRANS.round()
current.to_pickle("symbols_data/current")




current = current[-1:]
count = 0
status_cont=0
predictions = pd.DataFrame(index=("POS","NEG","DIR","PRED_POS","PRED_NEG","AVG_PROB_POS","AVG_PROB_NEG","PRED_DATE"),columns = symbols)

for symbol in symbols:
    status = current.iloc[:,count]
    status_prob = consecutives.loc[status,symbol]

    status_pos = consecutives.loc[1,symbol]
    status_neg = consecutives.loc[-1,symbol]

    if status.values[0] == 1:
        status_cont = status+1
        status_rev = -1
        status_prob_before = consecutives.loc[status_cont,symbol] / status_pos
        prob_pos = status_prob_before
        prob_neg = 1-prob_pos


    elif status.values[0] == -1:
        status_cont = status-1
        status_rev = 1
        status_prob_before = consecutives.loc[status_cont,symbol] / status_neg
        prob_neg = status_prob_before
        prob_pos = 1-prob_neg

    elif status.values[0] >=1:
        status_cont = status+1
        status_rev = -1
        status_prob_before = (status_pos - status_prob) / status_neg
        prob_neg = status_prob_before
        prob_pos = 1-prob_neg
    elif status.values[0] <=-1:
        status_cont = status-1
        status_rev = 1
        status_prob_before = (status_neg - status_prob) / status_pos
        prob_pos = status_prob_before
        prob_neg = 1-prob_pos



    predictions.loc["POS",symbol] = prob_pos.values[0]
    predictions.loc["NEG",symbol] = prob_neg.values[0]
    if prob_pos.values[0]>0.5:
        predictions.loc["DIR",symbol] = 1
    else:
        predictions.loc["DIR",symbol] =-1
    predictions.loc["PRED_POS","^NDX"] = (predictions.loc["DIR"]>0).sum()
    predictions.loc["PRED_NEG","^NDX"] = (predictions.loc["DIR"]<0).sum()
    predictions.loc["AVG_PROB_POS","^NDX"] = (predictions.loc["POS"]).mean()
    predictions.loc["AVG_PROB_NEG","^NDX"] = (predictions.loc["NEG"]).mean()

 
    predictions.loc["PRED_DATE","^NDX"] = today

    count +=1
predictions.to_pickle("symbols_data/predictions")





soglia = 0.9 # treshhold

pos = (predictions.loc["POS"] > soglia)
neg = (predictions.loc["NEG"] > soglia)

sel_pos = predictions.loc["POS", pos].index.tolist()
sel_neg = predictions.loc["NEG", neg].index.tolist()

sel_pos_prob = predictions.loc["POS",sel_pos].mean()
sel_neg_prob = predictions.loc["NEG",sel_neg].mean()





sel_pos3 = pd.DataFrame(sel_pos)
sel_neg3 = pd.DataFrame(sel_neg)
sel_pos3.to_pickle("X_test/sel_pos3.pkl")
sel_neg3.to_pickle("X_test/sel_neg3.pkl")

end_time = datetime.datetime.now()

print('Duration: {}'.format(end_time - start_time))


# In[4]:


# for each symbol, statistic of consecutives negative or positive period,
#i.e. AAPL has 8.59% probability of 2 negative return consecutives month (21 trading days)
consecutives


# In[10]:


# for each symbol, statistic of last 20 months of performance
#i.e. ADI is now 4 months consecutively positive, VRTX is 2nd consecutive negative month
current


# In[11]:


# for each symbol, statistic of probability of the performance in the next 21 trading days
#+ average information from pred_pos (number of positive stocks), pred_neg , etc
predictions


# In[13]:


# stop loss and take profit for each stocks calculate the levels of exit (out of the money or in the money) to be applied in the next 21 trading days
# sel_pos3 and sel_neg3 are the stocks which probabilities (to go up or down) are above a certain threshold 

